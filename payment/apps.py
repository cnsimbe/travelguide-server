from __future__ import unicode_literals

from django.apps import AppConfig
from .api import payAPI_init

class PaymentConfig(AppConfig):
    name = 'payment'

    def ready(self):
        payAPI_init()