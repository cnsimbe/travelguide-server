
from django.utils import timezone
import time, json, paypalrestsdk
from .settings import notificationPath, paypal_mode, paypal_client_id, paypal_client_secret, paypal_client_currency
from tgserver.settings import SERVER_PUBLIC_ADDRESS
from threading import Thread
from django.views.decorators.csrf import csrf_exempt

@csrf_exempt
def paypalPay(paymentToken, transactions,payer_id):
    print("payerID: "+ str(paymentToken))
    payment = paypalrestsdk.Payment({
    "intent": "sale",
    "payer": {
        "payment_method": "credit_card",
        "funding_instruments": [{
            "credit_card_token": {
                "credit_card_id": paymentToken,
                "payer_id": str(payer_id)}}]},

    "transactions": transactions})
    if payment.create():
        return payment
    else:
        print("Payment Failed")
        print(payment.error)
    raise ValueError('Error Charging Credit Card')

def payAPI_init():
    paypalrestsdk.configure({
      "mode": paypal_mode, # sandbox or live
      "client_id": paypal_client_id,
      "client_secret": paypal_client_secret })

def getPaymentToken(data):
    credit_card = paypalrestsdk.CreditCard(data)

    if credit_card.create():
       return credit_card.id
    raise ValueError('Error Creating Credit Card')

