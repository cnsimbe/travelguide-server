# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-09-29 11:33
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('server', '0007_auto_20160929_1131'),
    ]

    operations = [
        migrations.AlterField(
            model_name='location',
            name='polygon',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, to='server.Polygon'),
        ),
    ]
