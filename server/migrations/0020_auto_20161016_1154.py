# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-10-16 18:54
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('server', '0019_auto_20161013_2033'),
    ]

    operations = [
        migrations.CreateModel(
            name='PointType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, null=True)),
            ],
        ),
        migrations.AddField(
            model_name='point',
            name='pointtypes',
            field=models.ManyToManyField(blank=True, to='server.PointType'),
        ),
    ]
