from django.shortcuts import render
from django.http import HttpResponse, JsonResponse,FileResponse,HttpResponseForbidden
from .models import *
from django.contrib.auth import authenticate, login, logout
from django.views.decorators.csrf import csrf_exempt
from tgserver.settings import APP_AUTH_HEADER
from appsession.api import userSession, endSession, startSession
from payment.api import getPaymentToken, paypalPay
from datetime import datetime
# Create your views here.

def index(request):
	return render(request,"server/index.html")

def getAuthUser(request, header=None):
	authHeader = "HTTP_" + APP_AUTH_HEADER.upper().replace("-", "_")
	if authHeader in request.META:
		user = userSession(request.META[authHeader])
		return user
	elif header is not None:
		user = userSession(header)
		return user
	return None






def isAuth(request):
	ans = False
	try:
		ans = request.user.is_authenticated and request.user.is_active==True
	except Exception:
		pass
	if ans == True:
		return True
	else:
		return False

def _login(request,user):
	login(request, user)
	return startSession(user, None).key

def _logout(request):
	endSession(request.user)
	logout(request)

@csrf_exempt
def isLoggedIn(request):
	request.user  = getAuthUser(request) or request.user
	return JsonResponse({"success":isAuth(request),"user":userToJSON(request.user)}, safe=False)

@csrf_exempt
def loginUser(request):
	request.user  = getAuthUser(request) or request.user
	is_staff = None
	if "is_staff" in request.POST:
		is_staff = request.POST["is_staff"]
		if is_staff == "true" or is_staff == "True" or  is_staff == True:
			is_staff = True
		else:
			is_staff = False
	user = authenticate(username=request.POST["username"], password=request.POST["password"])
	if user is not None and user.is_active==True and ( is_staff is None or user.is_staff == is_staff):
		authHeader = _login(request, user)
		return JsonResponse({"success":True,APP_AUTH_HEADER:authHeader, "user":userToJSON(user)}, safe=False)
	else:
		return JsonResponse({"success":False,APP_AUTH_HEADER:None, "user":{}}, safe=False)

@csrf_exempt
def logoutUser(request):
	request.user  = getAuthUser(request) or request.user
	_logout(request)
	return JsonResponse({"success":True}, safe=False)


@csrf_exempt
def remindPassword(request):
	request.user  = getAuthUser(request) or request.user
	pass

@csrf_exempt
def remindUserName(request):
	request.user  = getAuthUser(request) or request.user
	pass


@csrf_exempt
def changePassword(request):
	request.user  = getAuthUser(request) or request.user
	success = False
	data = request.POST
	user = authenticate(username=data["username"], password=data["oldpassword"])
	if user is not None and user.is_active==True:
		user.set_password(data["password"])
		user.save()
		success = True
	else:
		pass
	return JsonResponse({"success":success}, safe=False)


@csrf_exempt
def payment(request):
	data = {"success":False}
	try:
		request.user  = getAuthUser(request) or request.user
		locationToBuy = json.loads(request.body)
		location = Location.objects.get(id=locationToBuy["id"])
		if location.id != locationToBuy["id"] or float(location.price) != float(locationToBuy["price"]):
			data["success"] = False
			data["message"] = "Location Data has changed"
			raise ValueError("Data Error")
		if isAuth(request) and request.user.is_staff == False:
			if hasattr(request.user, "tourist") and request.user.tourist is not None:
				if request.user.tourist.paidFor(location)==True:
					print("User has paid for location")
					data["success"] = True
				else:
					transactionInfo = {"amount":{"total":str(location.price),"currency":"CAD"}, "description":"Payment by " + request.user.get_full_name() + " for area, " +location.name}
					#paypal transaction wont work in a production environment
					#recommended to switch to Stripe!
					payment = paypalPay(request.user.tourist.paymentToken, [transactionInfo], request.user.tourist.id)
					print("Payment info is")
					print(payment)
					if hasattr(payment, "payer"):
						print(payment.transactions)
						print(payment.transactions[0].amount["total"])
						userpayment =UserPayment.objects.create(user=request.user,isProcessed=True,location=location,paymentId=payment.id,paymentType=payment.payer.payment_method, paymentAmount=payment.transactions[0].amount["total"])
						print("User payment obtained is")
						print(userpayment.id)
						data["success"] = True


	except Exception as Exp:
		print(Exp)
	finally:
		return JsonResponse(data, safe=False)

def getLandingPage(request):
	image = LandingPage.objects.all()[0].image
	return JsonResponse({"success":True, "url": image.url}, safe=False)


@csrf_exempt
def signup(request):
	user = None
	tourist = None
	success = False
	try:
		request.user  = getAuthUser(request) or request.user
		data = json.loads(request.body)
		user = User()
		for key,value in data["user"].iteritems():
			if key in ["first_name", "username" ,"last_name", "email"]:
				setattr(user, key, value)
		user.set_password(data["user"]["password"])
		#user is automatically set to active
		user.is_active = True
		user.is_staff =  False
		user.is_superuser = False
		user.save()
		tourist = Tourist(user=user)
		success = tourist._update(data, user)
		is_staff = None
		if "is_staff" in request.POST:
			is_staff = request.POST["is_staff"]
			if is_staff == "true" or is_staff == "True" or  is_staff == True:
				is_staff = True
			else:
				is_staff = False
		if success == False or is_staff == True:
			user.delete()
			if success == True:
				tourist._delete()
			return JsonResponse({"success":success, "tourist":{}}, safe=False)
		else:
			print("Tourist created: " + str(success))
			if success == True:
				data["paymentInfo"]["payer_id"] = tourist.pk
				tourist.paymentToken = getPaymentToken(data["paymentInfo"])
				tourist.save()
			return JsonResponse({"success":success, "tourist":tourist.toJson()}, safe=False)
	except Exception as Exp:
		if user is not None and user.id is not None:
			user.delete()
		if tourist is not None and tourist.id is not None:
			tourist._delete()
		return JsonResponse({"success":False, "tourist":{}}, safe=False)

@csrf_exempt
def userLocations(request):
	request.user  = getAuthUser(request) or request.user
	data = []
	if isAuth(request) and request.user.is_staff == False:
		if hasattr(request.user, "tourist") and request.user.tourist is not None:
			data = request.user.tourist.getLocations()
	else:
		pass
	return JsonResponse(data, safe=False)

@csrf_exempt
def userLocationAudio(request):
	header = None
	if "APP_AUTH_HEADER" in request.GET:
		header = request.GET["APP_AUTH_HEADER"]
	request.user  = getAuthUser(request, header) or request.user
	response = None
	if isAuth(request):
		point = Point.objects.get(pk=request.GET["id"])
		fileIterator, content_type = point.getAudioFileIterator()
		if fileIterator is None:
			response = HttpResponse()
		elif request.user == point.location.adminuser or request.user.is_superuser == True:
			response = FileResponse(fileIterator, content_type=content_type)
			response['Content-Disposition'] = 'attachment; filename=%s' % fileIterator.name
		elif hasattr(request.user, "tourist") and request.user.tourist is not None:
			if request.user.tourist.paidFor(point.location)==True:
				response = FileResponse(fileIterator, content_type=content_type)
				response['Content-Disposition'] = 'attachment; filename=%s' %  fileIterator.name
	else:
		pass

	return response


@csrf_exempt
def userProfile(request):
	request.user  = getAuthUser(request) or request.user
	if request.method == 'GET':
		return getUserProfile(request)
	elif request.method == 'POST':
		return updateUserProfile(request)

@csrf_exempt
def pointTypes(request):
	request.user  = getAuthUser(request) or request.user
	if request.method == 'GET':
		return getPointTypes(request)
	elif request.method == 'POST':
		pass


@csrf_exempt
def adminUsers(request):
	request.user  = getAuthUser(request) or request.user
	if request.method == 'GET':
		return getAdminUsers(request)
	elif request.method == 'POST':
		return updateAdminUsers(request)

@csrf_exempt
def locations(request):
	request.user  = getAuthUser(request) or request.user
	if request.method == 'GET':
		return getLocations(request)
	elif request.method == 'POST':
		return updateLocation(request)
	elif request.method == 'DELETE':
		return deleteLocation(request)

@csrf_exempt
def locationPoints(request):
	request.user  = getAuthUser(request) or request.user
	if request.method == 'GET':
		return getLocationPoints(request)
	elif request.method == 'POST':
		return updateLocationPoint(request)
	elif request.method == 'DELETE':
		return deleteLocationPoint(request)

@csrf_exempt
def locationPolygons(request):
	if request.method == 'GET':
		return getLocationPolygons(request)
	elif request.method == 'POST':
		return updateLocationPolygon(request)

@csrf_exempt
def adminPayments(request):
	request.user  = getAuthUser(request) or request.user
	if request.method == 'GET':
		return getAdminPayments(request)

#Get methods

def getAdminPayments(request):
	params = {}
	params["startdate"] = request.GET["startdate"]
	params["enddate"] = request.GET["enddate"]
	params["locations"] = [int(i) for i in request.GET.getlist("locations")]
	params["users"] = [int(i) for i in request.GET.getlist("users")]

	_authed  = isAuth(request)
	locationIds = params["locations"]
	userIds = params["users"]
	startdate = datetime.fromtimestamp(int(params["startdate"])/1000.0)
	enddate = datetime.fromtimestamp(int(params["enddate"])/1000.0)

	if not (_authed and request.user.is_staff == True):
		return HttpResponseForbidden("UnAuthorized")

	if request.user.is_superuser==True:
		userpayments = UserPayment.objects.filter(startDate__range=[startdate, enddate],location__adminuser__in=userIds,location__in=locationIds)
	elif request.user.is_staff == True:
		userpayments = UserPayment.objects.filter(startDate__range=[startdate, enddate],location__adminuser=request.user,location__in=locationIds)
	
	data = [] #{location:location, amdinusers:[{user:user,payments:[payment]}]}
	for i in userpayments:
		entry = filter(lambda x: x["location"]["id"]==i.location.id, data)
		if len(entry)==0:
			entry = {"location":i.location.toJson(),"adminusers":[]}
			data.append(entry)
		else:
			entry =  entry[0]

		adminuser = filter(lambda x: x["user"]["id"]==i.location.adminuser.id, entry["adminusers"])

		if len(adminuser)==0:
			adminuser = {"user":userToJSON(i.location.adminuser),"payment":{"total":0}}
			entry["adminusers"].append(adminuser)
		else:
			adminuser =  adminuser[0]

		
		if i.isProcessed==True:
			adminuser["payment"]["total"] += (i.paymentAmount or 0)
	return JsonResponse(data, safe=False)

def getLocationPolygons(request):
	data = []
	if isAuth(request) and request.user.is_superuser==True:
		polygons = Polygon.objects.all()
		for l in polygons:
			data.append(l.toJson())
	return JsonResponse(data, safe=False)

def getPointTypes(request):
	data = []
	for l in PointType.objects.all():
		data.append(l.toJson())
	return JsonResponse(data, safe=False)

def getLocations(request):
	_authed  = isAuth(request)
	if _authed and request.user.is_superuser==True:
		locationList = Location.objects.all()
	elif _authed and request.user.is_staff == True:
		locationList = Location.objects.filter(adminuser=request.user)
	else:
		locationList = Location.objects.filter(visibility=True)
	data = []
	for l in locationList:
		data.append(l.toJson())
	return JsonResponse(data, safe=False)

def getLocationPoints(request):
	location = None
	data = []
	try:
		if isAuth(request):
			if request.user.is_superuser==True:
				location = Location.objects.get(pk=request.GET["id"])
			elif request.user.is_staff==True:
				location = Location.objects.get(pk=request.GET["id"], adminuser=request.user)
			elif hasattr(request.user, "tourist") and request.user.tourist is not None:
				location = Location.objects.get(pk=request.GET["id"], visibility=True)
				if request.user.tourist.paidFor(location) == False:
					location = None
			for p in location.point_set.all():
				data.append(p.toJson())
	except Exception as Exp:
		print(Exp)
	finally:
		return JsonResponse(data, safe=False)



def getUserProfile(request):
	data = {}
	if isAuth(request) and request.user.is_staff == False:
		if hasattr(request.user, "tourist") and request.user.tourist is not None:
			data = request.user.tourist.toJson()
	else:
		pass
	return JsonResponse(data,safe=False)


def getAdminUsers(request):
	data = []
	if isAuth(request) and request.user.is_superuser==True:
		users = User.objects.filter(is_staff=True)
		for user in users:
			data.append(userToJSON(user))
	return JsonResponse(data, safe=False)


#update-create methods

def updateLocation(request):
	success = False
	data =  json.loads(request.body)
	response = {}
	try:
		_authed  = isAuth(request)
		if _authed and request.user.is_superuser==True:
			if data["id"] is not None:
				location = Location.objects.get(pk=data["id"])
			else:
				location = Location()
			success = location._update(data, request.user)
			response = location.toJson()
		elif _authed and request.user.is_staff == True:
			if data["id"] is not None:
				location = Location.objects.get(pk=data["id"], adminuser=request.user)
				success = location._update(data, request.user)
				response = location.toJson()
		else:
			print("Unauthorized")
	except Exception as Exp:
		print(Exp)
	finally:
		return JsonResponse({"success":success, "data":response}, safe=False)

def updateLocationPolygon(request):
	success = False
	response = {}
	data =  json.loads(request.body)
	try:
		if isAuth(request) and request.user.is_superuser==True:
			if data["id"] is not None:
				locationPolygon = Polygon.objects.get(pk=data["id"])
			else:
				locationPolygon = Polygon()
			success = locationPolygon._update(data, response.user)
			response = locationPolygon.toJson()
	finally:
		return JsonResponse({"success":success, "data":response}, safe=False)

def updateLocationPoint(request):
	success = False
	audioFile = None
	response = {}
	
	if len(request.FILES) > 0:
		data =  json.loads(request.POST["data"])
	else:
		data = json.loads(request.body)
	try:
		_authed = isAuth(request)
		if _authed and request.user.is_staff:
			if request.user.is_superuser==True:
				if data["id"] is not None:
					locationPoint = Point.objects.get(pk=data["id"])
				else:
					locationPoint = Point()
			elif request.user.is_staff==True:
				if data["id"] is not None:
					locationPoint = Point.objects.get(pk=data["id"], location__adminuser=request.user)
				else:
					locationPoint = Point(location=request.user.location_set.get(id=data["location"]["id"]))
			if "audioFile" in request.FILES:
				audioFile = request.FILES["audioFile"]
			success = locationPoint._update(data, request.user ,audioFile)
			response = locationPoint.toJson()
		else:
			pass
	except Exception as Exp:
		print(Exp)
	finally:
		return JsonResponse({"success":success, "data":response}, safe=False)

def updateUserProfile(request):
	success = False
	response = {}
	data = json.loads(request.body)
	if isAuth(request) and request.user.is_staff==False:
		if hasattr(request.user, "tourist") and request.user.tourist is not None:
			data["id"] = request.user.tourist.pk
			success = request.user.tourist._update(data, request.user)
			response = request.user.tourist.toJson()
	else:
		pass
	return JsonResponse({"success":success, "data":response}, safe=False)

@csrf_exempt
def updateAdminUsers(request):
	data =  json.loads(request.body)
	if isAuth(request) and request.user.is_superuser == True:
		if "id" in data and data["id"] is not None:
			user  = User.objects.get(id=data["id"])
		else:
			user = User()
		for key,value in data.iteritems():
			if (key == "id" or key == "pk")  and value is None:
				continue
			elif key in ["is_superuser", "is_active", "is_staff"]:
				if value == "true" or value == True or value == "True":
					setattr(user, key, True)
				elif value == "false" or value == False or value == "False":
					setattr(user, key, False)
			elif key == "password":
				if  value is not None and value !='':
					user.set_password(value)
			else:
				setattr(user, key, value)
		user.save()
		return JsonResponse({"success":True, "data":userToJSON(user)}, safe=False)
	else:
		return JsonResponse({"success":False, "data":{}}, safe=False)
	
	

#delete methods

def deleteLocation(request):
	success = False
	data =  json.loads(request.body)
	try:
		if isAuth(request) and request.user.is_superuser==True:
			location = Location.objects.get(pk=data["id"])
			success = location._delete()
	finally:
		return JsonResponse({"success":success}, safe=False)

def deleteLocationPoint(request):
	success = False
	audioFile = None
	data =  json.loads(request.body)
	try:
		if isAuth(request) and request.user.is_staff==True:
			if request.user.is_superuser:
				locationPoint = Point.objects.get(pk=data["id"])
			else:
				locationPoint = Point.objects.get(pk=data["id"], location__adminuser=request.user)
			success = locationPoint._delete()
		else:
			print("UnAthorized")
	finally:
		return JsonResponse({"success":success}, safe=False)



