from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from . import views

urlpatterns = [
	#url(r'^$', views.index, name='index'),

	url(r'^payment$', views.payment, name='payment'),
	url(r'^adminPayments$', views.adminPayments, name='adminPayments'),

	url(r'^isLoggedIn$', views.isLoggedIn, name='isLoggedIn'),
	url(r'^loginUser$', views.loginUser, name='loginUser'),
	url(r'^logoutUser$', views.logoutUser, name='logoutUser'),
	url(r'^changePassword$', views.changePassword, name='changePassword'),
	url(r'^signup$', views.signup, name='signup'),
	
	url(r'^userLocations$', views.userLocations, name='userLocations'),
	url(r'^userLocationAudio$', views.userLocationAudio, name='userLocationAudio'),
	url(r'^userProfile$', views.userProfile, name='userProfile'),
	
	url(r'^adminUsers$', views.adminUsers, name='adminUsers'),

	url(r'^pointTypes$', views.pointTypes, name='pointTypes'),

    url(r'^locations$', views.locations, name='locations'),
    url(r'^locationPoints$', views.locationPoints, name='locationPoints'),
    url(r'^locationPolygons$', views.locationPolygons, name='locationPolygons'),
    url(r'^getLandingPage$', views.getLandingPage, name='landingPage'),
] + static(settings.MEDIA_URL + "landingPage/", document_root=settings.MEDIA_ROOT + "/landingPage")