from __future__ import unicode_literals
import json
from django.utils import timezone
from django.contrib.auth.models import User
from django.db import models
from django.db.models import Q
from django.utils.dateformat import format
from django.core.urlresolvers import reverse




def paymentEndDate():
	return timezone.now() + timezone.timedelta(days=366)

def audioFileLocation(instance, filename):
	return str(instance.location.pk) + "/" + str(instance.pk) + "/" + filename

def landingPageLocation(instance, filename):
	return "landingPage/" + str(instance.pk) + "/" + filename

# Create your models here.

class Polygon(models.Model):
	points = models.CharField(max_length=1000,null=True)
	created = models.DateTimeField(auto_now_add=True, editable=False, null=True)
	last_updated = models.DateTimeField(auto_now=True, editable=False, null=True)
	strokeColor = models.CharField(max_length=255, null=True, blank=True)
	strokeWeight = models.IntegerField(null=True, blank=True)
	strokeOpacity = models.IntegerField(null=True, blank=True)
	fillColor = models.CharField(max_length=255, null=True, blank=True)
	fillOpacity = models.IntegerField(null=True, blank=True)
	def getPoints(self):
		return json.loads(self.points)

	def _update(self, data, user):
		if self.id != data["id"] or user.is_superuser == False:
			return False
		for key,value in data.iteritems():
			if key == "id" or key == "pk":
				continue
			elif key == "points":
				setattr(self, key, json.dumps(value))
			else:
				setattr(self, key, value)
		self.save()
		return True

	def toJson(self):
		data = {"id": self.pk,"strokeColor":self.strokeColor,"strokeWeight":self.strokeWeight, "strokeOpacity":self.strokeOpacity, "fillColor":self.fillColor, "fillOpacity":self.fillOpacity, "points":self.getPoints()}
		return data

	def __str__(self):
		if hasattr(self, "location"):
			return str(self.location)
		else:
			return "Polygon - " + str(self.pk)


class LandingPage(models.Model):
	image = models.FileField(null=True, blank=True, upload_to=landingPageLocation)

class Location(models.Model):
	name = models.CharField(max_length=255,null=True)
	created = models.DateTimeField(auto_now_add=True, editable=False, null=True)
	last_updated = models.DateTimeField(auto_now=True, editable=False, null=True)
	isReady = models.BooleanField(default=False)
	adminuser = models.ForeignKey(User, null=True)
	lat = models.DecimalField(decimal_places=19, max_digits=23,null=True)
	lng = models.DecimalField(decimal_places=19, max_digits=23,null=True)
	price = models.DecimalField(decimal_places=2, max_digits=12,null=True)
	visibility = models.BooleanField(default=True)
	description = models.CharField(max_length=1000,null=True)
	polygon = models.OneToOneField(Polygon, models.PROTECT ,null=True)
	zoom = models.IntegerField(null=True)

	def _update(self, data, user):
		adminEdits = ["description"] 
		if self.id != data["id"] or  user.is_staff == False:
			return False
		for key,value in data.iteritems():
			if user.is_superuser == False and key not in adminEdits:
				continue
			if key == "id" or key == "pk" or key == "num_points" or key == "adminuser":
				continue
				

			elif key == "polygon":
				if self.polygon is None or self.polygon._update(value, user) == False:
					if value["id"] is not None:
						polygon = Polygon.objects.get_or_create(id=value["id"])[0]
					else:
						polygon = Polygon()
					if polygon._update(value, user) == True:
						self.polygon = polygon
			else:
				setattr(self, key, value)
		self.save()
		if user.is_superuser == True:
			updateAdminActiveTime(self, data["adminuser"])
		return True

	def _delete(self):
		self.delete()
		return True

	def toJson(self):
		user = userToJSON(self.adminuser)
		if "id" not in user:
			user["id"] = None;
		data = {"id": self.pk,"name":self.name, "adminuser":user  ,"isReady":self.isReady,"price":self.price,"lat":self.lat, "lng":self.lng ,"num_points": len(self.point_set.all()) ,"visibility":self.visibility, "zoom":self.zoom, "description":self.description,"polygon" : self.polygon.toJson()}
		return data

	def __str__(self):
		return self.name


class PointType(models.Model):
	name = models.CharField(max_length=255, null=True)
	def toJson(self):
		return {"id":self.id, "name":self.name}

	def __str__(self):
		return self.name

class Point(models.Model):
	location = models.ForeignKey(Location, models.CASCADE, null=True)
	pointtypes =  models.ManyToManyField(PointType, blank=True)
	created = models.DateTimeField(auto_now_add=True, editable=False, null=True)
	last_updated = models.DateTimeField(auto_now=True, editable=False, null=True)
	lat = models.DecimalField(decimal_places=19, max_digits=23,null=True)
	lng = models.DecimalField(decimal_places=19, max_digits=23,null=True)
	name = models.CharField(max_length=255,null=True)
	visibility =  models.BooleanField(default=True)
	radius = models.DecimalField(decimal_places=4, max_digits=10,null=True)
	audioFile = models.FileField(null=True, blank=True, upload_to=audioFileLocation)
	content_type = models.CharField(max_length=255,null=True, blank=True)
	def _update(self, data, user ,audioFile=None):

		adminEdits = ["lat", "lng", "name", "visibility","radius", "audioFile", "content_type" ] 
		if self.id != data["id"] or user.is_staff == False:
			return False
		for key,value in data.iteritems():
			if user.is_superuser == False and key not in adminEdits:
				continue
			if  key=="audioFile":
				if value == False and self.audioFile is not None:
					self.audioFile.delete()
					self.content_type =  None
			elif key == "id" or key == "pk":
				continue
			elif key == "location":
				if self.location is None or self.location._update(value, user) == False:
					if value["id"] is not None:
						location = Location.objects.get_or_create(id=value["id"])[0]
					else:
						location = Location()
					if location._update(value, user) == True:
						print(location.id)
						self.location = location
			elif key == "pointtypes":
				pass
			else:
				setattr(self, key, value)
		
		if audioFile is not None:
			self.audioFile.save(audioFile.name, audioFile)
			self.content_type =  audioFile.content_type
			
		self.save()

		if "pointtypes" in data:
			newtypes = [ int(i["id"]) for i in data["pointtypes"] ]
			newtypes = [ PointType.objects.get(id=i) for i in newtypes  ]
			todelete = list( set(self.pointtypes.all()) - set(newtypes) )

			toadd = list(set(newtypes) - set(self.pointtypes.all()) )
			print(toadd)
			for i in todelete:
				self.pointtypes.remove(i)

			for i in toadd:
				self.pointtypes.add(i)
					
		self.save()
		return True

	def _delete(self):
		self.delete()
		return True

	def getAudioFileIterator(self):
		return self.audioFile, self.content_type

	def toJson(self):
		audioFile = None
		if hasattr(self.audioFile, "url")==True:
			audioFile =  { "name":self.audioFile.name, "content_type":self.content_type ,"url" : (reverse('userLocationAudio') + '?id=' + str(self.id))}
		data = {"id": self.pk,"name":self.name, "pointtypes":[] ,"visibility":self.visibility, "radius":self.radius ,"location":self.location.toJson(), "lat":self.lat, "lng":self.lng, "audioFile":audioFile}
		for i in self.pointtypes.all():
			data["pointtypes"].append(i.toJson())
		return data

	def __str__(self):
		return self.name +  str(self.pk)


class Tourist(models.Model):
	user =  models.OneToOneField(User)
	paymentToken =  models.CharField(max_length=1000, null=True)
	created = models.DateTimeField(auto_now_add=True, editable=False, null=True)
	last_updated = models.DateTimeField(auto_now=True, editable=False, null=True)
	def getLocations(self):
		data = []
		payments = UserPayment.objects.filter(Q(isProcessed=True) & Q(user=self.user) & (Q(endDate=None) | Q(endDate__gt=timezone.now())))
		for p  in payments:
			if len(filter(lambda x: x["id"] == p.location.id, data))==0:
				data.append(p.location.toJson())
		return data

	def _update(self, data, user, paymentToken=None):
		if self.id != data["id"] or (user.is_staff == True and user.is_superuser==False):
			return False
		for key,value in data.iteritems():
			if key in ["id","pk","created","last_updated", "paymentToken"]:
				continue
			elif key == "user":
				if self.user is not None:
					for key2,value2 in value.iteritems():
						if key2 in ["first_name", "last_name", "email"]:
							setattr(self.user, key2, value2)
					self.user.save()
			else:
				setattr(self, key, value)
		if paymentToken is not None:
			self.paymentToken = paymentToken
		self.save()
		return True

	def toJson(self):
		data = {"id":self.pk,"created":format(self.created, "U"), "last_updated":format(self.last_updated, "U")}
		data["user"] = userToJSON(self.user)
		return data

	def paidFor(self, location):
		payments = UserPayment.objects.filter(Q(user__pk=self.user.pk) & Q(location__pk=location.pk) & Q(isProcessed=True) & (Q(endDate=None) | Q(endDate__gt=timezone.now())))
		return len(payments) > 0

	def _delete(self):
		if self.user is not None and self.user.id is not None:
			self.user.delete()
		if self.id is not None:
			self.delete()
	def __str__(self):
		return self.user.get_full_name()


class UserPayment(models.Model):
	user =  models.ForeignKey(User)
	startDate = models.DateTimeField(null=True, auto_now=True)
	endDate = models.DateTimeField(null=True,default=paymentEndDate)
	isProcessed = models.BooleanField(default=False)
	location = models.ForeignKey(Location, models.PROTECT)
	paymentId =  models.CharField(max_length=1000, null=True)
	paymentType = models.CharField(max_length=255, null=True)
	paymentAmount = models.DecimalField(decimal_places=2, max_digits=6,null=True,blank=True)

	def __str__(self):
		return self.user.get_full_name() + ' ' + str(self.location)

class AdminActiveTime(models.Model):
	user =  models.ForeignKey(User, null=True)
	startDate = models.DateTimeField()
	endDate = models.DateTimeField(null=True)
	location = models.ForeignKey(Location, models.PROTECT)

	def __str__(self):
		return self.user.get_full_name() + str(self.startDate)


def updateAdminActiveTime(location, newuser):
	if (newuser["id"]  is None and location.adminuser is None) or (newuser["id"] is not None and location.adminuser is not None and location.adminuser.id == newuser["id"]):
		return
	else:
		if location.adminuser is not None:
			try:
				activeTime = location.adminuser.adminactivetime_set.get(endDate=None)
				activeTime.endDate = timezone.now()
				activeTime.save()
			except Exception as Exp:
				pass
		if newuser["id"] is not None:
			try:
				user = User.objects.get(id=newuser["id"])
				if user.is_staff is not True:
					return
				activeTime = user.adminactivetime_set.create(user=user,startDate=timezone.now(),location=location,endDate=None)
				location.adminuser = user
				location.save()
			except Exception as Exp:
				pass

def userToJSON(user):
	l = {}
	try:
		l["id"] = user.id
		l["pk"] = user.pk
		l["email"] = user.email
		l["username"] = user.username
		l["first_name"] = user.first_name
		l["last_name"] = user.last_name
		l["is_superuser"] = user.is_superuser
		l["is_staff"] = user.is_staff
		l["is_active"] = user.is_active
	finally:
		return l