# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-10-01 02:16
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('appsession', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sessionmodel',
            name='key',
            field=models.CharField(max_length=1000, unique=True),
        ),
        migrations.AlterField(
            model_name='sessionmodel',
            name='userid',
            field=models.IntegerField(),
        ),
    ]
