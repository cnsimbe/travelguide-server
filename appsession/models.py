from __future__ import unicode_literals

from django.db import models

# Create your models here.

class sessionModel(models.Model):
	key = models.CharField(max_length=1000, unique=True)
	expires = models.DateTimeField(null=True)
	userid = models.IntegerField()