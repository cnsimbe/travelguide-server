from .models import sessionModel
from django.contrib.auth.models import User
from django.db.models import Q
from django.utils import timezone
from .settings import sessionAge
from datetime import timedelta
import uuid

def startSession(user, age=0):
	endSession(user)
	sm = sessionModel()
	try:
		sm.userid = user.pk
		if age == 0:
			sm.expires = timezone.now() + timedelta(seconds=sessionAge)
		else:
			sm.expires = None
		sm.key = uuid.uuid4()
		sm.save()
	finally:
		return sm

def endSession(user):
	try:
		sm = sessionModel.objects.filter(userid=user.pk)
		sm.delete()
	except Exception as exp:
		pass

def userSession(key):
	try:
		sm = sessionModel.objects.get(Q(key=key) & (Q(expires=None) | Q(expires__gt=timezone.now()) ))
		return User.objects.get(pk=sm.userid)
	except Exception as exp:
		return None
